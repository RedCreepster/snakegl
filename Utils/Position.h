#ifndef SNAKEGL_POSITION_H
#define SNAKEGL_POSITION_H

class Position {
private:
    float posX = 0;
    float posY = 0;
public:
    explicit Position() = default;

    explicit Position(float posX, float posY) : posX(posX), posY(posY) {}

    float getPosX() const {
        return posX;
    }

    void setPosX(float posX) {
        Position::posX = posX;
    }

    float getPosY() const {
        return posY;
    }

    void setPosY(float posY) {
        Position::posY = posY;
    }
};

#endif //SNAKEGL_POSITION_H
