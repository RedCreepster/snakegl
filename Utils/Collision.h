#ifndef SNAKEGL_COLLISION_H
#define SNAKEGL_COLLISION_H

template<typename T>
class Collision {
public:
    virtual bool isCollisionTo(const T &another)= 0;
};

#endif //SNAKEGL_COLLISION_H
