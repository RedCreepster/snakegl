#ifndef SNAKEGL_DRAWABLE_H
#define SNAKEGL_DRAWABLE_H

class Drawable {
    /**
     * Отрисовка фигуры
     */
    virtual void draw()=0;
};

#endif //SNAKEGL_DRAWABLE_H
