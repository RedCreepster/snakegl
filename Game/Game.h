#ifndef SNAKEGL_GAME_H
#define SNAKEGL_GAME_H

#include "Snake/Snake.h"

#define SNAKEGL_DIRECTION_UP        1
#define SNAKEGL_DIRECTION_DOWN      2
#define SNAKEGL_DIRECTION_LEFT      3
#define SNAKEGL_DIRECTION_RIGHT     4

#ifndef START_SNAKE_SIZE
#define START_SNAKE_SIZE            3
#endif

class Game {
private:
    Snake snake = Snake(START_SNAKE_SIZE);
    Circle egg = Circle(0, 0, SNAKE_HEAD_RADIUS);
    bool eggFound = false;
    int lastDirection = SNAKEGL_DIRECTION_UP;
public:
    void init();

    void nextStep(int direction);

    void draw();

    void generateEgg();

    bool isGameOver();

    bool isWin();

    void addTail();

private:
    int getValidDirection(int direction);

    vector<Position> findEmptyTailForEgg();
};


#endif //SNAKEGL_GAME_H
