#include <cstdlib>
#include "Game.h"

void Game::init() {
    float x = 10 * SNAKE_BODY_WIDTH + SNAKE_BODY_WIDTH / 2;
    float y = 10 * SNAKE_BODY_HEIGHT + SNAKE_BODY_HEIGHT / 2;
    snake = Snake(START_SNAKE_SIZE, Position(x, y));
}

int Game::getValidDirection(int direction) {
    if (direction == SNAKEGL_DIRECTION_UP && lastDirection == SNAKEGL_DIRECTION_DOWN) {
        direction = SNAKEGL_DIRECTION_DOWN;
    }
    if (direction == SNAKEGL_DIRECTION_DOWN && lastDirection == SNAKEGL_DIRECTION_UP) {
        direction = SNAKEGL_DIRECTION_UP;
    }
    if (direction == SNAKEGL_DIRECTION_LEFT && lastDirection == SNAKEGL_DIRECTION_RIGHT) {
        direction = SNAKEGL_DIRECTION_RIGHT;
    }
    if (direction == SNAKEGL_DIRECTION_RIGHT && lastDirection == SNAKEGL_DIRECTION_LEFT) {
        direction = SNAKEGL_DIRECTION_LEFT;
    }
    lastDirection = direction;
    return direction;
}

void Game::nextStep(int direction) {
    direction = getValidDirection(direction);
    switch (direction) {
        case SNAKEGL_DIRECTION_UP:
            snake.up();
            break;
        case SNAKEGL_DIRECTION_DOWN:
            snake.down();
            break;
        case SNAKEGL_DIRECTION_LEFT:
            snake.left();
            break;
        case SNAKEGL_DIRECTION_RIGHT:
            snake.right();
            break;
        default:
            break;
    }

    if (snake.isCollisionTo(egg)) {
        snake.addTail();
        eggFound = false;
    }

    if (!eggFound) {
        generateEgg();
    }
}

void Game::draw() {
    snake.draw();
    if (eggFound) {
        egg.draw();
    }
}

void Game::addTail() {
    snake.addTail();
}

void Game::generateEgg() {
    int x = ((int) random()) % 512;
    int y = ((int) random()) % 512;

    auto *xz = new Circle(x, y, SNAKE_HEAD_RADIUS);
    xz->setColor(0, 1, 1);
    while (snake.isCollisionTo(*xz)) {
        x = ((int) random()) % 512;
        y = ((int) random()) % 512;
        xz->setPosX(x);
        xz->setPosY(y);
    }

    egg = *xz;
    eggFound = true;
}

vector<Position> Game::findEmptyTailForEgg() {
//    vector<Position> notEmptyTiles;
//    notEmptyTiles.push_back(snake.head);
//    for (int i = 0; i < 512 / TAIL_WIDTH; ++i) {
//        for (int j = 0; j < 512 / TAIL_HEIGHT; ++j) {
//
//        }
//    }
    return {Position(30, 50), Position(50, 30)};
}
