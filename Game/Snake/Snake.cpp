#include <iostream>
#include "Snake.h"
#include "../Game.h"

Snake::Snake(int size, Position position) : head(Head(position)) {
    for (int i = 0; i < size; ++i) {
        float bodyX = i * SNAKE_BODY_WIDTH + SNAKE_HEAD_RADIUS + position.getPosX();
        float bodyY = position.getPosY() - SNAKE_HEAD_RADIUS;
        Body rectangle = Body(bodyX, bodyY);
        rectangle.setColor(1, i * .5f, i * .5f);
        this->body.push_back(rectangle);
    }
}

void Snake::draw() {
    head.draw();

    for (auto &i : body) {
        i.draw();
    }
}

void Snake::moveTail(int direction) {
    float headX = head.getPosX();
    float headY = head.getPosY();

    float deltaX = 0;
    float deltaY = 0;

    switch (direction) {
        case SNAKEGL_DIRECTION_UP:
            deltaX = -0.5f;
            deltaY = 0.5f;
            break;
        case SNAKEGL_DIRECTION_DOWN:
            deltaX = -0.5f;
            deltaY = -1.5f;
            break;
        case SNAKEGL_DIRECTION_LEFT:
            deltaX = 0.5f;
            deltaY = -0.5f;
            break;
        case SNAKEGL_DIRECTION_RIGHT:
            deltaX = -1.5f;
            deltaY = -0.5f;
            break;
        default:
            break;
    }

    if (!addBody) {
        float lastX = 0;
        float lastY = 0;
        for (auto &i : body) {
            if (lastX != 0 && lastY != 0) {
                float tailX = i.getPosX();
                float tailY = i.getPosY();
                i.setPosX(lastX);
                i.setPosY(lastY);
                lastX = tailX;
                lastY = tailY;
            } else {
                lastX = i.getPosX();
                lastY = i.getPosY();
            }
        }

        Body &value = body.front();
        float bodyX = headX + SNAKE_BODY_WIDTH * deltaX;
        float bodyY = headY + SNAKE_BODY_HEIGHT * deltaY;
        value.setPosX(bodyX);
        value.setPosY(bodyY);
    } else {
        float r = 1.0;
        float g = 0;
        float b = 0;

        float bodyX = headX + SNAKE_BODY_WIDTH * deltaX;
        float bodyY = headY + SNAKE_BODY_HEIGHT * deltaY;
        Body value = Body(bodyX, bodyY);
        value.setColor(r, g, b);

        body.insert(body.begin(), value);
        addBody = false;
    }
}

void Snake::addTail() {
    addBody = true;
}

void Snake::up() {
    float d = head.getPosY() - head.getR() * 2;
    if (d < 0) {
        d = 512 - SNAKE_BODY_HEIGHT / 2;
    }
    head.setPosY(d);
    moveTail(SNAKEGL_DIRECTION_UP);
}

void Snake::down() {
    float d = head.getPosY() + head.getR() * 2;
    if (d > 512) {
        d = SNAKE_BODY_HEIGHT / 2;
    }
    head.setPosY(d);
    moveTail(SNAKEGL_DIRECTION_DOWN);
}

void Snake::left() {
    float d = head.getPosX() - head.getR() * 2;
    if (d < 0) {
        d = 512 - SNAKE_BODY_WIDTH / 2;
    }
    head.setPosX(d);
    moveTail(SNAKEGL_DIRECTION_LEFT);
}

void Snake::right() {
    float d = head.getPosX() + head.getR() * 2;
    if (d > 512) {
        d = SNAKE_BODY_WIDTH / 2;
    }
    head.setPosX(d);
    moveTail(SNAKEGL_DIRECTION_RIGHT);
}
