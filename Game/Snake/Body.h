#ifndef SNAKEGL_BODY_H
#define SNAKEGL_BODY_H

#include <cmath>
#include "../../Shapes/Rectangle.h"
#include "../../Shapes/Circle.h"
#include "Head.h"

#define SNAKE_BODY_WIDTH    16
#define SNAKE_BODY_HEIGHT   16

class Body : public Rectangle, Collision<Head> {
public:
    Body(float x, float y) : Rectangle(x, y, SNAKE_BODY_WIDTH, SNAKE_BODY_HEIGHT) {}

    bool isCollisionTo(const Head &another) override {
        return false;
    }
};

#endif //SNAKEGL_BODY_H
