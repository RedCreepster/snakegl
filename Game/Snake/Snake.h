#ifndef SNAKEGL_SNAKE_H
#define SNAKEGL_SNAKE_H

#include "../../Shapes/Shape.h"
#include "../../Shapes/Circle.h"
#include "../../Shapes/Rectangle.h"
#include "Body.h"
#include "Head.h"
#include <vector>

using namespace std;

class Snake : public Drawable, Collision<Circle> {
protected:
    Head head;
    vector<Body> body;
public:
    explicit Snake(int size) : Snake(size, Position(0, 0)) {}

    explicit Snake(int size, Position position);

    void draw() override;

    /**
     * Добавить к хвосту одну ячейку в следующем ходу
     */
    void addTail();

    /**
     * Переместиться на клетку вверх
     */
    void up();

    /**
     * Переместиться на клетку вниз
     */
    void down();

    /**
     * Переместиться на клетку влево
     */
    void left();

    /**
     * Переместиться на клетку вправо
     */
    void right();

    bool isCollisionTo(const Circle &another) override {
        return head.isCollisionTo(another);
    }

    bool isCollisionToMe() {
        for (auto i = body.begin() + 1; i != body.end(); ++i) {
            if (i->isCollisionTo(head)) {
                return true;
            }
        }
        return false;
    }

    vector<Position> notEmptyTiles() {
        vector<Position> notEmptyTiles;

//        notEmptyTiles.push_back(Position(head.x-SNAKE_HEAD_RADIUS,head.y-SNAKE_HEAD_RADIUS));

        return notEmptyTiles;
    }

private:
    /**
     * Переместить хвост за головой
     * @param direction Для определения дельты смещения тела
     */
    void moveTail(int direction);

    bool addBody = false;
};


#endif //SNAKEGL_SNAKE_H
