#ifndef SNAKEGL_HEAD_H
#define SNAKEGL_HEAD_H

#include "../../Shapes/Shape.h"
#include "../../Shapes/Circle.h"

#define SNAKE_HEAD_RADIUS   ((16 / 2 + 16 / 2) / 2)

class Head : public Circle {
public:
    explicit Head(const Position &position) : Circle(position.getPosX(), position.getPosY(), SNAKE_HEAD_RADIUS) {}
};

#endif //SNAKEGL_HEAD_H
