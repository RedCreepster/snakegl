#include <GL/glut.h>
#include <iostream>
#include <cstring>
#include "Game/Game.h"

float WinWid = 512.0;
float WinHei = 512.0;
bool isFullScreen = false;
bool pause = false;
uint timerDelay = 500;
unsigned int direction = SNAKEGL_DIRECTION_UP;
Game *game;

void Draw() {
    glClear(GL_COLOR_BUFFER_BIT);

    game->draw();

    glutSwapBuffers();
}

void Timer(int /*unused*/) {
    if (!pause) {
        game->nextStep(direction);
    }

    glutPostRedisplay();
    glutTimerFunc(timerDelay, Timer, 0);
}

void keyboard(unsigned char key, int /*unused*/, int /*unused*/) {
    std::cout << glutGetModifiers() << " + " << (int) key << "\n";
    switch (key) {
        case 27:
            exit(0);
        case 13:
            if ((glutGetModifiers() & GLUT_ACTIVE_ALT) != 0) {
                if (isFullScreen) {
                    glutReshapeWindow((int) WinWid, (int) WinHei);
                } else {
                    glutFullScreen();
                }
                isFullScreen = !isFullScreen;
            }
            break;
        case 'a':
            game->addTail();
            break;
        case ' ':
            pause = !pause;
            break;
        case '+':
            timerDelay = max<uint>(timerDelay - 50, 50);
            break;
        case '-':
            timerDelay = min<uint>(timerDelay + 50, 600);
            break;
        default:
            break;
    }
}

void sKeyboard(int key, int /*unused*/, int /*unused*/) {
    switch (key) {
        case GLUT_KEY_LEFT:
            direction = SNAKEGL_DIRECTION_LEFT;
            break;
        case GLUT_KEY_RIGHT:
            direction = SNAKEGL_DIRECTION_RIGHT;
            break;
        case GLUT_KEY_UP:
            direction = SNAKEGL_DIRECTION_UP;
            break;
        case GLUT_KEY_DOWN:
            direction = SNAKEGL_DIRECTION_DOWN;
            break;
        default:
            break;
    }
}

void Initialize() {
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WinWid, WinHei, 0, -200.0, 200.0);
    glMatrixMode(GL_MODELVIEW);

    game = new Game();
    game->init();
}

int main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize((int) WinWid, (int) WinHei);
    glutInitWindowPosition(100, 200);
    glutCreateWindow("SnakeGL");
    glutDisplayFunc(Draw);
    glutTimerFunc(timerDelay, Timer, 0);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(sKeyboard);
    Initialize();
    glutMainLoop();
    return 0;
}