#include <GL/gl.h>
#include "Rectangle.h"

Rectangle::Rectangle(float x, float y, float width, float height)
        : ColoredShape(x, y, 1, 0, 0, 1), width(width), height(height) {
}

void Rectangle::draw() {
    glBegin(GL_POLYGON);

    glVertex2f(getPosX(), getPosY());
    glVertex2f(getPosX() + this->width, getPosY());
    glVertex2f(getPosX() + this->width, getPosY() + this->height);
    glVertex2f(getPosX(), getPosY() + this->height);

    glDrawElements(GL_POLYGON, 6, GL_UNSIGNED_INT, nullptr);
    glEnd();
}

float Rectangle::getWidth() const {
    return width;
}

void Rectangle::setWidth(float width) {
    Rectangle::width = width;
}

float Rectangle::getHeight() const {
    return height;
}

void Rectangle::setHeight(float height) {
    Rectangle::height = height;
}
