#ifndef SNAKEGL_COLOREDSHAPE_H
#define SNAKEGL_COLOREDSHAPE_H

#include "Shape.h"

/**
 * @deprecated Use TexturedShape
 */
class ColoredShape : public Shape {
protected:
    float red;
    float green;
    float blue;
    float alpha;
public:
    ColoredShape(float x, float y, float r, float g, float b, float alpha)
            : Shape(x, y), red(r), green(g), blue(b), alpha(alpha) {}

    void setColor(float red, float green, float blue) {
        setColor(red, green, blue, this->alpha);
    }

    void setColor(float red, float green, float blue, float alpha) {
        this->red = red;
        this->green = green;
        this->blue = blue;
        this->alpha = alpha;
    }

    float getRed() const {
        return red;
    }

    float getGreen() const {
        return green;
    }

    float getBlue() const {
        return blue;
    }

    float getAlpha() const {
        return alpha;
    }
};

#endif //SNAKEGL_COLOREDSHAPE_H
