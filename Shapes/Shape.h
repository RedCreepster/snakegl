#ifndef SNAKEGL_SHAPE_H
#define SNAKEGL_SHAPE_H

#include "../Utils/Position.h"
#include "../Utils/Drawable.h"

class Shape : public Position, Drawable {
public:
    Shape(float x, float y) : Position(x, y) {}
};

#endif //SNAKEGL_SHAPE_H
