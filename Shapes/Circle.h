#ifndef SNAKEGL_CIRCLE_H
#define SNAKEGL_CIRCLE_H

#include "ColoredShape.h"
#include "../Utils/Collision.h"

class Circle : public ColoredShape, Collision<Circle> {
public:
    Circle(float x, float y, float r);

    void draw() override;

    float getR() const;

    void setR(float r);

    bool isCollisionTo(const Circle &another) override;

protected:
    float r;
};


#endif //SNAKEGL_CIRCLE_H
