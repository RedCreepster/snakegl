#ifndef SNAKEGL_RECTANGLE_H
#define SNAKEGL_RECTANGLE_H

#include "ColoredShape.h"

class Rectangle: public ColoredShape {
public:
    Rectangle(float x, float y,float width, float height);

    void draw() override;

    float getWidth() const;

    void setWidth(float width);

    float getHeight() const;

    void setHeight(float height);

protected:
    float width;
    float height;

};

#endif //SNAKEGL_RECTANGLE_H
