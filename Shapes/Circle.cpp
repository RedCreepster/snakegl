#include <GL/gl.h>
#include <cmath>
#include "Circle.h"

Circle::Circle(float x, float y, float r)
        : ColoredShape(x, y, 0, 0, 0, 1), r(r) {}

void Circle::draw() {
    glColor3f(r, green, blue);
    glBegin(GL_POLYGON);
    for (float i = 0.0; i < 2 * M_PI; i += M_PI / 18.0) {
        double x = this->getPosX() + this->r * sinf(i);
        double y = this->getPosY() + this->r * cosf(i);
        glVertex2f((GLfloat) x, (GLfloat) y);
    }
    glEnd();
}

float Circle::getR() const {
    return r;
}

void Circle::setR(float r) {
    Circle::r = r;
}

bool Circle::isCollisionTo(const Circle &another) {
    float x1 = getPosX();
    float x2 = another.getPosX();
    float y1 = getPosY();
    float y2 = another.getPosY();
    double distance = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));

    return distance <= getR() + another.getR();
}
