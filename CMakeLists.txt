cmake_minimum_required(VERSION 3.7)
project(SnakeGL)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp
        Utils/Position.h
        Utils/Drawable.h
        Shapes/Shape.h
        Shapes/ColoredShape.h
        Shapes/Rectangle.cpp Shapes/Rectangle.h
        Shapes/Circle.cpp Shapes/Circle.h
        Utils/Collision.h
        Game/Snake/Head.h
        Game/Snake/Body.h
        Game/Snake/Snake.cpp Game/Snake/Snake.h
        Game/Game.cpp Game/Game.h)

find_package(GLUT REQUIRED)
include_directories(${GLUT_INCLUDE_DIRS})
link_directories(${GLUT_LIBRARY_DIRS})
add_definitions(${GLUT_DEFINITIONS})

if (NOT GLUT_FOUND)
    message(ERROR " GLUT not found!")
endif (NOT GLUT_FOUND)

find_package(OpenGL REQUIRED)
include_directories(${OpenGL_INCLUDE_DIRS})
link_directories(${OpenGL_LIBRARY_DIRS})
add_definitions(${OpenGL_DEFINITIONS})
if (NOT OPENGL_FOUND)
    message(ERROR " OPENGL not found!")
endif (NOT OPENGL_FOUND)

add_executable(SnakeGL ${SOURCE_FILES})

target_link_libraries(SnakeGL ${OPENGL_LIBRARIES} ${GLUT_LIBRARY} ${PNG_LIBRARY})
